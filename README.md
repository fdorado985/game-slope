<p align="center">
  <img src=".assets/logo.png" title="Logo">
</p>

---

![platform](.assets/platform.png)
[![ide](.assets/ide.png)](https://unity3d.com)
[![twitter](.assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](.assets/instagram.png)](https://www.instagram.com/juan_fdorado)

This repo will have some developed games as a practice of Unity 2018, where you can be able to see that is separated through different courses and sections, going from the basics to the most advanced features inside Unity.

## Game Development With Unity 2018
* [Intro to Unity](projects/Roll-A-Zombie) - Learn the basics of Unity creating **Roll-A-Zombie**.
