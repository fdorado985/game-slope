﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    // Properties

    public int selectedZombiePosition;
    public GameObject selectedZombie;
    public List<GameObject> zombies;
    public Vector3 selectedSize;
    public Vector3 defaultSize;
    public Text text;
    private int score;

    // View cycle

    void Start() {
        SelectedZombie(zombies[0], 0);
        text.text = "Score: " + score;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            ShiftLeft();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            ShiftRight();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            PushUp();
        }
    }

    // Functions

    void SelectedZombie(GameObject newZombie, int newPosition) {
        selectedZombie.transform.localScale = defaultSize;
        selectedZombie = newZombie;
        selectedZombie.transform.localScale = selectedSize;
        selectedZombiePosition = newPosition;
    }

    void ShiftLeft() {
        if (selectedZombiePosition == 0) {
            SelectedZombie(zombies[3], 3);
        } else {
            SelectedZombie(zombies[selectedZombiePosition - 1], selectedZombiePosition - 1);
        }
    }

    void ShiftRight() {
        if (selectedZombiePosition == 3) {
            SelectedZombie(zombies[0], 0);
        } else {
            SelectedZombie(zombies[selectedZombiePosition + 1], selectedZombiePosition + 1);
        }
    }

    void PushUp() {
        Rigidbody rb = selectedZombie.GetComponent<Rigidbody>();
        rb.AddForce(0, 0, 10, ForceMode.Impulse);
    }

    public void AddScore() {
      score = score + 1;
      text.text = "Score: " + score;
    }
}
