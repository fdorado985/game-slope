﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

	// Properties

	public GameManager gameManager;
	public AudioClip hit;
	private AudioSource source;

	private void Start() {
		source = GetComponent<AudioSource>();
	}

	// Functions
	
	private void OnTriggerEnter(Collider other) {
		gameManager.AddScore();
		source.PlayOneShot(hit);
	}

}
