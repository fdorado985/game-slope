# Roll-A-Zombie
This game helps to learn the basics of game development...
You will see how works with:
* `Collisions`
* `Music`
* `Sounds on collisions`
* `Gravity`
* `Material`
* `Axis for elements on scene`
* `Detect key press`
* `Canvas`
* `Forces`

## Demo
![roll_a_zombie_demo](.screenshots/roll_a_zombie_demo.gif)
